#ifndef PLAYER_H
#define PLAYER_H
#include <Input.h>
#include <PlayerState.h>
#include <AnimatedSprite.h>

class Player
{
private:
	PlayerState * m_state;
	AnimatedSprite m_animated_sprite;
public:
	Player(const AnimatedSprite&);
	virtual void handleInput(Input);
	virtual void update();
	AnimatedSprite& getAnimatedSprite();
};
#endif

