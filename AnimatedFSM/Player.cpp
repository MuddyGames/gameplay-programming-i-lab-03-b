#include <Player.h>

#include <IdlePlayerState.h>


Player::Player(const AnimatedSprite& sprite) : m_animated_sprite(sprite)
{
	m_state = new IdlePlayerState();
}

void Player::handleInput(Input input) {
	PlayerState * state = m_state->handleInput(input);

	if (state != NULL) {
		m_state->exit(*this);
		delete m_state;
		m_state = state;
		m_state->enter(*this);
	}
}

void Player::update() {
	m_animated_sprite.update();
	m_state->update(*this);
}

AnimatedSprite& Player::getAnimatedSprite() {
	int frame = m_animated_sprite.getCurrentFrame();
	m_animated_sprite.setTextureRect(m_animated_sprite.getFrame(frame));
	return m_animated_sprite;
}
