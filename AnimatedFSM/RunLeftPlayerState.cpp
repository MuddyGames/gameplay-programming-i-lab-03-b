#include <RunLeftPlayerState.h>

#include <IdlePlayerState.h>
#include <RunRightPlayerState.h>
#include <ClimbLadderPlayerState.h>

PlayerState* RunLeftPlayerState::handleInput(Input& input)
{
	if (input.getCurrent() == Input::Action::LEFT_RELEASE)
	{
		DEBUG_MSG("RunLeftPlayerState -> IdlePlayerState");
		return new IdlePlayerState();
	}
	else if (input.getCurrent() == Input::Action::HIT_WALL_EVENT)
	{
		DEBUG_MSG("RunLeftPlayerState -> IdlePlayerState");
		return new IdlePlayerState();
	}
	else if (input.getCurrent() == Input::Action::RIGHT_PRESS)
	{
		DEBUG_MSG("RunLeftPlayerState -> RunRightPlayerState");
		return new RunRightPlayerState();
	}
	else if (input.getCurrent() == Input::Action::HIT_LADDER_BOTTOM_EVENT)
	{
		DEBUG_MSG("RunLeftPlayerState -> ClimbLadderPlayerState");
		return new ClimbLadderPlayerState();
	}
	return nullptr;
}
void RunLeftPlayerState::update(Player& player) {}
void RunLeftPlayerState::enter(Player& player) 
{
	DEBUG_MSG("Entering RunLeftPlayerState");
}
void RunLeftPlayerState::exit(Player& player)
{
	DEBUG_MSG("Exiting RunLeftPlayerState");
}