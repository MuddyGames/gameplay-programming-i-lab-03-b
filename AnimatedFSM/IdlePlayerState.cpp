#include <IdlePlayerState.h>

#include <RunRightPlayerState.h>
#include <RunLeftPlayerState.h>


PlayerState* IdlePlayerState::handleInput(Input& input) {
	if (input.getCurrent() == Input::Action::LEFT_PRESS) {
		DEBUG_MSG("IdlePlayerState -> RunLeftPlayerState");
		return new RunLeftPlayerState();
	}
	else if (input.getCurrent() == Input::Action::RIGHT_PRESS) {
		DEBUG_MSG("IdlePlayerState -> RunRightPlayerState");
		return new RunRightPlayerState();
	}
	return nullptr;
}

void IdlePlayerState::update(Player& player) {}

void IdlePlayerState::enter(Player& player) 
{
	DEBUG_MSG("Entering IdlePlayerState");
}

void IdlePlayerState::exit(Player& player)
{
	DEBUG_MSG("Exiting IdlePlayerState");
}