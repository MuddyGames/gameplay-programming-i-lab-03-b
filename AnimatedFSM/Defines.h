#ifndef DEFINES_H
#define DEFINES_H

const char* const SPRITE_IDLE		= "assets\\Idle.png";
const char* const SPRITE_RUN_RIGHT	= "assets\\RunRight.png";
const char* const SPRITE_RUN_LEFT	= "assets\\RunLeft.png";
const char* const SPRITE_CLIMB		= "assets\\Climb.png";
const char* const SPRITE_GLIDE		= "assets\\Glide.png";
const char* const SPRITE_ATTACK		= "assets\\Attack.png";
const char* const SPRITE_SLIDE		= "assets\\Slide.png";
const char* const SPRITE_THROW		= "assets\\Throw.png";
const char* const SPRITE_JUMP		= "assets\\Jump.png";
const char* const SPRITE_JUMP_ATTACK= "assets\\JumpAttack.png";
const char* const SPRITE_JUMP_THROW = "assets\\JumpThrow.png";
const char* const SPRITE_Dead		= "assets\\Dead.png";


#endif
