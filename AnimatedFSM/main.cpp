#include <Defines.h>

#include <iostream>
#include <SFML/Graphics.hpp>
#include <Player.h>
#include <Input.h>
#include <Debug.h>

using namespace std;

int main()
{
	// Create the main window
	sf::RenderWindow window(sf::VideoMode(800, 600), "Player FSM");

	// Load a sprite to display
	sf::Texture idle_texture;
	if (!idle_texture.loadFromFile("assets\\Idle.png")) {
		DEBUG_MSG("Failed to load file");
		return EXIT_FAILURE;
	}

	// Setup Players Default Animated Sprite
	AnimatedSprite idle_animated_sprite(idle_texture);
	idle_animated_sprite.addFrame(sf::IntRect(0, 0, 232, 439));
	idle_animated_sprite.addFrame(sf::IntRect(232, 0, 232, 439));
	idle_animated_sprite.addFrame(sf::IntRect(464, 0, 232, 439));
	idle_animated_sprite.addFrame(sf::IntRect(0, 439, 232, 439));
	idle_animated_sprite.addFrame(sf::IntRect(232, 439, 232, 439));
	idle_animated_sprite.addFrame(sf::IntRect(464, 439, 232, 439));
	idle_animated_sprite.addFrame(sf::IntRect(0, 878, 232, 439));
	idle_animated_sprite.addFrame(sf::IntRect(232, 878, 232, 439));
	idle_animated_sprite.addFrame(sf::IntRect(464, 878, 232, 439));
	idle_animated_sprite.addFrame(sf::IntRect(0, 1317, 232, 439));

	idle_animated_sprite.setTime(seconds(0.05f));

	Player player(idle_animated_sprite);

	Input input;

	// Start the game loop
	while (window.isOpen())
	{
		// Process events
		sf::Event event;
		while (window.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::Closed:
				// Close window : exit
				window.close();
				break;
				// Just deals with Keypressed
				// Should also deal with Key Released
			case sf::Event::KeyPressed:
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
				{
					input.setCurrent(Input::Action::LEFT_PRESS);
				}
				else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
				{
					input.setCurrent(Input::Action::RIGHT_PRESS);
				}
				else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
				{
					input.setCurrent(Input::Action::UP_PRESS);
				}
				break;
			case sf::Event::KeyReleased:
				if (event.key.code == sf::Keyboard::Left)
				{
					input.setCurrent(Input::Action::LEFT_RELEASE);
				}
				else if (event.key.code == sf::Keyboard::Right)
				{
					input.setCurrent(Input::Action::RIGHT_RELEASE);
				}
				else if (event.key.code == sf::Keyboard::Up)
				{
					input.setCurrent(Input::Action::UP_RELEASE);
				}
				break;
			default:
				input.setCurrent(Input::Action::NONE);
				break;
			}
		}

		// Handle input to Player
		player.handleInput(input);

		// Update the Player
		player.update();

		// Clear screen
		window.clear();

		// Draw the Players Current Animated Sprite
		window.draw(player.getAnimatedSprite());

		// Update the window
		window.display();
	}

	return EXIT_SUCCESS;
};